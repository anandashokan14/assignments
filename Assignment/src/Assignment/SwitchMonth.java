package Assignment;

import java.util.Scanner;

public class SwitchMonth {

	public static void main(String[] args) {
		System.out.println("\tCalander Program");
		System.out.println("----------------------------");
		
		System.out.println("1.Jan \t 2.Feb \t 3.Mar");
		System.out.println("4.Apr \t 5.May \t 6.Jun");
		System.out.println("7.Jul \t 8.Aug \t 9.Sep");
		System.out.println("10.Oct \t 11.Nov\t 12.Dec");
		
		System.out.println("---------------------------");
		
		System.out.println("Enter Your Choice: ");
		
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		System.out.println("----------------------");
		
		System.out.println("Your Choice is : ");
		switch (num) {
			case 1 :
				System.out.println("January");
				break;
			case 2: 
				System.out.println("February");
				break;
			case 3: 
				System.out.println("March");
				break;
			case 4: 
				System.out.println("April");
				break;
			case 5: 
				System.out.println("May");
				break;
			case 6: 
				System.out.println("June");
				break;
			case 7: 
				System.out.println("July");
				break;
			case 8: 
				System.out.println("August");
				break;
			case 9: 
				System.out.println("Septmeber");
				break;
			case 10: 
				System.out.println("October");
				break;
			case 11: 
				System.out.println("November");
				break;
			case 12: 
				System.out.println("December");
				break;
			default :
				System.out.println("Invalid Choice!! Please select Right one!!");
				
		}
	}	

}
