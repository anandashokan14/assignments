package com.stackroute.basics;

import java.util.Scanner;

public class PhoneNumberValidator {
    //Create Scanner object as instance variable
	Scanner sc=new Scanner(System.in);
	
    public static void main(String[] args) {
        //call the functions in the required sequence
    	String input =new PhoneNumberValidator().getInput();
    	new PhoneNumberValidator().closeScanner();
    	int result=new PhoneNumberValidator().validatePhoneNumber(input);
    	new PhoneNumberValidator().displayResult(result);
    	
    }

    public String getInput() {
    	System.out.println("Enter the Phone Number: ");
    	String input=sc.next();
        return input;
    }

    public void displayResult(int result) {
        //displays the result
    	if(result==1) {
    		System.out.println("Valid");
    	}else {
    		System.out.println("Invalid");
    	}
    }

    public int validatePhoneNumber(String input) {
    	int total=0;
    	if (input=="" || input == null) {
    		return -1;
    	}
    	else {
    		char[] phoneNumber = input.toCharArray();
    		for(int i=0;i<phoneNumber.length;i++) {
    			char ch = phoneNumber[i];
    			if (ch >= '0' && ch <= '9' || ch =='-') {
    				
    				total++;
    			}
    			else {
    				return 0;
    			}
    		}
    		if (total==12) {
    			return 1;
    		}
    		else {
    			return 0;
    		}
    	}
    	
        
    }
    public void closeScanner(){
        //close the Scanner object
    	sc.close();
    }
}
